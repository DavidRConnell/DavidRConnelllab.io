+++
title = "Alzheimer's disease gene meta-analysis"
author = ["David R. Connell"]
layout = "single"
lastmod = 2021-01-28T00:03:46-06:00
draft = false
weight = 100
+++

## <span class="section-num">1</span> Introduction {#introduction}

see ([Barabási, Gulbahce, and Loscalzo 2010](#orgb370ece)) introduction for some extra motives.


## <span class="section-num">2</span> Data acquisition {#data-acquisition}

Instead of saving the data files to the git repository, the locations of files are listed in <kbd>./data/locations.txt</kbd> then the <kbd>getdata.sh</kbd> bash script will iterate through the list and download any files that don't exist locally in <kbd>./data/</kbd>.

<a id="listing--list:downloaddata"></a>
```bash
../src/getdata.sh
```

<div class="src-block-caption">
  <span class="src-block-number"><a href="#listing--list:downloaddata">Listing 1</a></span>:
  Download data files to data directory.
</div>

<br />

```text
Downloading data files...
protein-coding_gene.txt
All files downloaded successfully.
```


## <span class="section-num">3</span> Data processing {#data-processing}

<a id="listing--list:load-libraries"></a>
```R
library(tidyverse)
library(tidygraph)
library(ggraph)
```

<div class="src-block-caption">
  <span class="src-block-number"><a href="#listing--list:load-libraries">Listing 2</a></span>:
  Load libraries used in the analysis: <a href="https://www.tidyverse.org/">tidyverse</a>, <a href="https://tidygraph.data-imaginist.com/">tidygraph</a>, and <a href="https://ggraph.data-imaginist.com/">ggraph</a>.
</div>

<br />

<a id="listing--list:read-data"></a>
```R
data_dir <- "../data"
lit <- read_tsv(paste(data_dir, "Num_literature.txt", sep = "/")) %>%
  select(c("V3", "AD_co", "N")) %>%
  mutate(V3 = factor(V3))

node_data <- read_tsv(paste(data_dir, "protein-coding_gene.txt", sep = "/"),
  col_types =
    cols(
      kznf_gene_catalog = col_double(),
      intermediate_filament_db = col_character(),
      bioparadigms_slc = col_character(),
      horde_id = col_character(),
      lncrnadb = col_character()
    )
) %>%
  select(c("entrez_id", "gene_family", "gene_family_id", "location")) %>%
  extract(
    location,
    c("chromosome", "long_arm_p", "position"),
    regex = "(X|Y|\\d+)(q|p)([\\d\\.]+)",
    convert = TRUE
  ) %>%
  extract(gene_family_id, "gene_family_id", regex = "(\\d+)") %>%
  mutate(
    entrez_id = factor(entrez_id),
    chromosome = case_when(
      chromosome == "X" ~ "23",
      chromosome == "Y" ~ "24",
      TRUE ~ chromosome
    ),
    chromosome = parse_integer(chromosome),
    long_arm_p = if_else(long_arm_p == "q", 1, -1),
  ) %>%
  left_join(lit, by = c("entrez_id" = "V3"))
rm(lit)

ppi <- read_tsv(paste(data_dir, "PPI_entrez.txt", sep = "/")) %>%
  as_tbl_graph(directed = FALSE) %>%
  activate(nodes) %>%
  left_join(lit, by = c("name" = "V3")) %>%
  left_join(node_data, by = c("name" = "entrez_id")) %>%
  mutate(AD_co_ratio = AD_co / sum(AD_co, na.rm = TRUE)) %>%
  activate(edges) %>%
  filter(to != from)
rm(lit)
```

<div class="src-block-caption">
  <span class="src-block-number"><a href="#listing--list:read-data">Listing 3</a></span>:
  Read in data from the literature and protein-protein interactions and combine them into a single graph.
</div>

<br />

<a id="listing--list:create-edge-features"></a>
```R
ppi <- ppi %>%
  activate(edges) %>%
  mutate(
    closeness = if_else(
      (.N()[from, ]$chromosome == .N()[to, ]$chromosome) &
	(.N()[from, ]$long_arm_p == .N()[to, ]$long_arm_p),
      1 / ((.N()[from, ]$position - .N()[to, ]$position)^2),
      0
    ),
    closeness = if_else(closeness == Inf,
      100, closeness
    )
  )
```

<br />
Need to find way to work with multiple gene\_family\_ids. For the moment removing all but the first.

The AD co-ratio redefined as the proportion of all Alzheimer's disease papers the given gene is in.
The previous definition normalized the number of AD papers the gene was by the total number of papers the gene was in.
This shows specificity of the gene but a gene might be important to multiple disease in which case even if it has a large affect on AD it will still be found in non-AD papers reducing its ratio.
If a gene has only been looked at in a few papers but all those papers were related to AD it will have a high ratio.
Assumingly, genes that appear to be important to AD will be in a larger than normal proportion of all AD papers.

Genes that impact AD will likely interact with other AD genes.
To find new genes of interest first find genes that are highly associated with AD already and look at their characteristics and interaction graphs.
Missed AD disease genes may be highly connected to those genes.
One damaged gene could alter the behavior of any of the genes it interacts with.

<a id="listing--list:find-high-ad-genes"></a>
```R
high_ad_ppi <- ppi %>%
  activate(nodes) %>%
  filter(AD_co_ratio >= 0.001) %>%
  mutate(community = group_louvain(),
	 between = centrality_betweenness(),
	 degree = centrality_degree())
```

<div class="src-block-caption">
  <span class="src-block-number"><a href="#listing--list:find-high-ad-genes">Listing 4</a></span>:
  Filter genes to find only those with a high AD co-ratio.
</div>

<br />
Communities are found within the high AD co-ratio graph to remove nodes outside of the main network to simplify the analysis.


### <span class="section-num">3.1</span> Node features {#node-features}

-   Literature values
-   Structural value/families/superfamilies


### <span class="section-num">3.2</span> Graphs {#graphs}

-   PPI
-   Closeness


### <span class="section-num">3.3</span> Labels {#labels}

-   P-values/effect sizes from GWAS


## <span class="section-num">4</span> Network training {#network-training}

<a id="listing--save-node-table"></a>
```R
ppi %>%
  activate(nodes) %>%
  as_tibble() %>%
  select(c("gene_family_id", "AD_co_ratio")) %>%
  replace_na(list(gene_family_id = "-99",
		  AD_co_ratio = 0))
```

<a id="listing--save-edge-table"></a>
```R
ppi %>%
  activate(edges) %>%
  as_tibble() %>%
  replace_na(list(closeness = 0)) # Not close is probably a good assumption.
```

<a id="listing--list:add-ad-module"></a>
```python
import sys
sys.path.append('../src')
```

<div class="src-block-caption">
  <span class="src-block-number"><a href="#listing--list:add-ad-module">Listing 5</a></span>:
  Add the <a href="https://gitlab.com/DavidRConnell/DavidRConnell.gitlab.io/-/tree/master/src/adprediction">Ad prediction</a>  package to python's path.
</div>

<a id="listing--list:load-dataset"></a>
```python
from adprediction.read import LoadDataset
from spektral.transforms import GCNFilter, AdjToSpTensor

dataset = LoadDataset(nodes, edges, n_label_classes=10, transforms = [GCNFilter(), AdjToSpTensor()])
```

<br />
Note: See [Pooling layers - Spektral](https://graphneural.network/layers/pooling/#diffpool) for how to take advantage of edge features. Also look at convolutional layers that include edge features and pooling layers may reduce node features as opposed to using a smaller set of protein families.

<a id="listing--list:train-model"></a>
```python
from adprediction.model import GeneGCN
from spektral.data import SingleLoader
from tensorflow.keras.layers import Input
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.regularizers import l2

learning_rate = 1e-2
n_training_epochs = 50
n_hidden_states = 16

model = GeneGCN(n_hidden_states, dataset.n_labels)
model.compile(Adam(lr=learning_rate), loss="categorical_crossentropy",weighted_metrics=['acc'])

loader_train = SingleLoader(dataset, sample_weights=dataset.training_mask)
model.fit(
    loader_train.load(),
    steps_per_epoch=loader_train.steps_per_epoch,
    epochs=n_training_epochs,
)

model.summary()
```

<a id="listing--list:test-model"></a>
```python
loader_test = SingleLoader(dataset, sample_weights=dataset.testing_mask)
eval_results = model.evaluate(loader_test.load(), steps=loader_test.steps_per_epoch)
print("Model results:\n" "Test loss: {}\n" "Test accuracy:{}".format(*eval_results))
```

<a id="listing--list:one-hot-2-class"></a>
```python
def one_hot_2_class(one_hot_vecs):
    """ Convert a vector of one-hot vectors to a vector of class numbers.

    If the vectors are predictions (such as the output from softmax), uses
    the maximum value to determine the guess.
    """

    return one_hot_vecs.argmax(axis=1)
```

<a id="listing--list:predict-genes"></a>
```python
loader_predict = SingleLoader(dataset, sample_weights=dataset.to_predict_mask)
predictions = model.predict(loader_predict.load(), steps=loader_predict.steps_per_epoch)

original = one_hot_2_class(dataset[0].y[np.logical_not(dataset.to_predict_mask)])
predicted = one_hot_2_class(predictions[np.logical_not(dataset.to_predict_mask)])

np.savetxt("../cache/predicted.txt", np.array([original, predicted]).T, fmt="%d",delimiter="\t", header="original\tpredicted", comments="")
```

```R
read_tsv("../cache/predicted.txt") %>%
  count(original, predicted) %>%
  mutate(
    original = factor(original),
    predicted = factor(predicted)
  ) %>%
  ggplot(aes(x = original, y = predicted, fill = n)) +
  geom_tile(color = "white") +
  theme_minimal() +
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank())
```

{{< figure src="/images/model_prediction.svg" >}}


## <span class="section-num">5</span> References {#references}

<a id="orgb370ece"></a>Barabási, Albert-László, Natali Gulbahce, and Joseph Loscalzo. 2010. “Network Medicine: A Network-Based Approach to Human Disease.” _Nature Reviews Genetics_ 12 (1):56–68. <https://doi.org/10.1038/nrg2918>.