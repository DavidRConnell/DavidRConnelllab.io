import pandas as pd
import numpy as np
from numpy import matlib as ml
import scipy.sparse as sp
from spektral.data import Dataset, Graph


class LoadDataset(Dataset):
    """A graph dataset of protein-protein interactions as edges and
    protein metadata as node features. Labels are relative AD
    co-values from the literature.

    Parameters:
        node_file (str) - A tab delimited file containing node features.
        edge_file (str) - A tab delimited file containing edges in the form from-to.
        split (float) - Proportion of labels to use for training.
        n_label_classes (int) - Number of classes to bin labels into.
    """

    def __init__(self, node_file, edge_file, split=0.8, n_label_classes=10, **kwargs):
        self.node_file = node_file
        self.edge_file = edge_file
        self.n_label_classes = n_label_classes
        super().__init__(**kwargs)

        self.split = split
        (
            self.training_mask,
            self.testing_mask,
            self.to_predict_mask,
        ) = self._generate_masks()

    def read(self):
        nodes = pd.read_table(self.node_file, header=None).to_numpy().astype(np.float32)
        edges = pd.read_table(self.edge_file, header=None).to_numpy()

        edge_features = edges[:, -1].astype(np.float32)
        edges = edges[:, :-1].astype(int)

        edges -= 1  # Python indexes from 0.
        a = np.zeros((nodes.shape[0], nodes.shape[0]), dtype=np.float32)
        e = np.zeros((nodes.shape[0] * nodes.shape[0], 1), dtype=np.float32)
        for i in range(edges.shape[0]):
            a[edges[i, 0], edges[i, 1]] = 1
            a[edges[i, 1], edges[i, 0]] = 1
            e[edges[i, 0] + edges[i, 1]] = edge_features[i]  # Row-wise indexing.
            e[edges[i, 1] + edges[i, 0]] = edge_features[i]

        edges = sp.csr_matrix(a)
        labels = nodes[:, -1]
        nodes = nodes[:, :-1]

        labels = self._one_hot_vectorize_labels(labels)
        nodes = self._one_hot_vectorize_nodes(nodes)

        return [Graph(x=nodes, a=edges, e=e, y=labels)]

    def _one_hot_vectorize_nodes(self, nodes):
        """Convert categorical node features to one-hot-vectors.

        Features 0 and 1 are categorical. Ideally should have a better
        way of identifying them than hard coding the indices.
        """

        def convert(vec):
            unique_values = np.unique(vec)
            new_vecs = np.zeros(
                (vec.shape[0], unique_values.shape[0]), dtype=np.float32
            )

            for (i, j) in enumerate(vec):
                new_vecs[i, np.where(unique_values == j)[0][0]] = 1

            return new_vecs

        new_hot_vector = convert(nodes)

        return new_hot_vector

    def _one_hot_vectorize_labels(self, labels):
        """Convert labels to a one hot vector.

        Since labels are continous values they must first be binned
        based on the value supplied to *self.n_label_classes*. A label
        of zero is treated as "unknown" and does not get a bin. For
        these the final vector will be all false.
        """

        sigmoid = lambda x: np.power(1 + np.exp(-x), -1)
        known_labels_idx = labels > 0
        labels[known_labels_idx] = np.log10(labels[known_labels_idx])
        labels[known_labels_idx] = sigmoid(
            labels[known_labels_idx] - (0.5 * np.median(labels[known_labels_idx]))
        )

        bins = np.linspace(
            np.min(labels[known_labels_idx]),
            np.max(labels[known_labels_idx]),
            self.n_label_classes + 1,
        )

        labels[np.logical_not(known_labels_idx)] = labels[known_labels_idx].min() - 1
        positions = ml.repmat(labels, self.n_label_classes, 1).T >= ml.repmat(
            bins[:-1], labels.shape[0], 1
        )

        positions = positions.sum(axis=1) - 1
        new_vecs = np.zeros((labels.shape[0], self.n_label_classes), dtype=bool)
        new_vecs[
            np.arange(labels.shape[0])[positions >= 0], positions[positions >= 0]
        ] = True

        return new_vecs

    def _generate_masks(self):
        """Create masks for training, testing, and predicting sets.

        Returns:
            training_mask (np.array): mask for training nodes.
            testing_mask (np.array): mask for testing nodes.
            to_predict_mask (np.array): mask for nodes who's values are
            not yet known and need to be predicted.
        """

        labels = self[0].y
        known_predicate = labels.sum(axis=1)
        to_predict_mask = np.logical_not(known_predicate)

        known_labels_idx = np.where(known_predicate)[0]
        shuffled_known_labels_idx = np.random.permutation(known_labels_idx)
        cut_off = np.floor(known_labels_idx.shape[0] * self.split).astype(int)

        def indices_2_mask(indices):
            mask = np.zeros((labels.shape[0]), dtype=bool)
            mask[indices] = True

            return mask

        training_mask = indices_2_mask(shuffled_known_labels_idx[:cut_off])
        testing_mask = indices_2_mask(shuffled_known_labels_idx[cut_off:])

        return (training_mask, testing_mask, to_predict_mask)
