from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dropout
from spektral.layers import GCNConv


class GeneGCN(Model):
    """Graph convolutional network for predicting labels for a PPI network.

    Parameters
        n_hidden (int): Number of hidden layers to use.
        n_label_classes (int): Number of layer classes (length of label one-hot vectors).
        dropout (float): Dropout rate (default 0.5).
        l2_reg (float): Rate for l2 regularization (default 2.5e-4).
    """

    def __init__(self, n_hidden, n_labels, dropout=0.5, l2_reg=2.5e-4):
        super().__init__()

        self.dropout = Dropout(0.5)
        self.graph_conv_1 = GCNConv(
            n_hidden, activation="relu", kernel_regularizer=l2(l2_reg), use_bias=False
        )
        self.graph_conv_2 = GCNConv(n_labels, activation="softmax", use_bias=False)

    def call(self, inputs):
        node_features, adj_matrix, edge_features = inputs
        out = self.dropout(node_features)
        out = self.graph_conv_1([out, adj_matrix])
        # Need more computer.
        # out = self.edge_conv([out, adj_matrix, edge_features])
        out = self.dropout(out)
        out = self.graph_conv_2([out, adj_matrix])

        return out
