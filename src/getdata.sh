#! /bin/bash

datadir=$(dirname "$0")/../data/
cd "$datadir" || exit 2

echo "Downloading data files..."
IFS="$(printf '\t')"
{
    read
    while read -r filename address; do
        echo "$filename"
        [ -f "$filename" ] || wget "$address" -O "$filename"
    done
} < locations.txt
echo "All files downloaded successfully."
